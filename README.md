# challenge_NSI

Documentation challenge NSI

# Package dependency 
```
beautifulsoup4             4.9.3
Brotli                     1.0.9
cffi                       1.15.0
click                      8.1.2
colorama                   0.4.4
csscompressor              0.9.5
cssselect2                 0.5.0
fonttools                  4.32.0
ghp-import                 2.0.2
html5lib                   1.1
htmlmin                    0.1.12
importlib-metadata         4.11.3
Jinja2                     3.1.1
jsmin                      3.0.1
libsass                    0.21.0
livereload                 2.6.3
Markdown                   3.3.6
MarkupSafe                 2.1.1
mergedeep                  1.3.4
mkdocs                     1.3.0
mkdocs-extra-sass-plugin   0.1.0
mkdocs-material            8.2.8
mkdocs-material-extensions 1.0.3
mkdocs-minify-plugin       0.5.0
mkdocs-redirects           1.0.4
mkdocs-with-pdf            0.9.3
packaging                  21.3
Pillow                     9.1.0
pip                        22.0.4
pycparser                  2.21
pydyf                      0.1.2
Pygments                   2.11.2
pymdown-extensions         9.3
pyparsing                  3.0.7
pyphen                     0.12.0
python-dateutil            2.8.2
PyYAML                     6.0
pyyaml_env_tag             0.1
qrcode                     7.3.1
setuptools                 58.1.0
six                        1.16.0
soupsieve                  2.3.2
tinycss2                   1.1.1
tornado                    6.1
watchdog                   2.1.7
weasyprint                 54.3
webencodings               0.5.1
zipp                       3.8.0
zopfli                     0.2.1
```

