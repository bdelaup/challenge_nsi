# Téléchargements


!!! info "Le programme"
    [![PDF](img/pdf.png){ align=left width="30"}](./pdf/challente_nsi-programme.pdf)  Télécharger le [programme](./pdf/challente_nsi-programme.pdf) de la journée.

!!! abstract "Le règlement"
    [![PDF](img/pdf.png){ align=left width="30"}](./pdf/challenge_nsi.pdf)  Télécharger la version [PDF](./pdf/challenge_nsi.pdf) de ce document.

!!! danger "Pack élève"
    [![ZIP](img/zip.png){ align=left width="30"}](./assets/eleve.zip)  Télécharger le [ZIP](./assets/eleve.zip) contenant les images et les fichiers python.

!!! info "Pack élève"
    [![ZIP](img/pdf.png){ align=left width="30"}](./pdf/challenge_nsi-rapport_activite.pdf)  Télécharger le [rapport d'activité](./pdf/challenge_nsi-rapport_activite.pdf).
    
