# Challenge 4 : Création d'un filtre créatif

| Originale  | ![arrow](../img/right_arrow.svg){ width="20"} | Transformée |
| ---------- | --------------- | ---------- |
|![Originale](img/champs_rgb.jpg){ width="100"}| ![arrow](../img/right_arrow.svg){ width="20"}  |![Transformée](img/creatif_champs_rgb.jpg){ width="100" }|


!!! Question "A faire"
    Créer un filtre permettant de modifier une image en lui appliquant un filtre créatif en vue d'obtenir un rendu original.
    *Ex* : couleur "flashy", flou, négatif...


!!! Tips "Démarche"
    1. Éditer le fichier ```fonctions.py``` 
    2. Ajouter la fonction ```couleur_vers_creatif``` 
    3. Ajouter au moins un test
    

!!! Example "Exemple" 
    ```python title="Prototype couleur_vers_creatif()" linenums="1"
    def couleur_vers_creatif(tableau_entree):
        """ 
        Transforme une image RVB en une autre image RVB.
        La fonction applique un filtre créatif "artistique".c
        tableau_entree : liste de tuple.
                            Chaque tuple code un pixel au format (R, V, B).

        retourne :       une liste de tuples codé au format (R, V, B)
        """

    ```

!!! bug "Test"
    Pour tester uniquement cette partie vous pouvez exécuter:
    ``` batch title="Terminal" linenums="1"
    python challenge.py -b
    ```


