# Challenge 1 : Niveau de gris 

| Originale  | ![arrow](../img/right_arrow.svg){ width="20"}  | Transformée |
| ---------- | --------------- | ---------- |
|![Originale](img/champs_rgb.jpg){ width="100"}| ![arrow](../img/right_arrow.svg){ width="20"}  |![Transformée](img/champs_ng.jpg){ width="100" }|

!!! Question "A faire"
    Transformer une image couleur au format RVB vers une image en niveau de gris.

!!! Tips "Démarche"
    1. Éditer le fichier ```fonctions.py``` 
    2. Ajouter la fonction ```transforme_vers_ng()``` 
    3. Ajouter au moins un test
    
!!! Example "Exemple"
    ```python title="Prototype transforme_vers_ng()" linenums="1"
    def transforme_vers_ng(tableau_entree):
        """ 
        Transforme une image RVB vers une image noir et blanc
        tableau_entree : liste de tuple.
                            Chaque tuple code un pixel au format (R, V, B).

        retourne :       une liste de pixels codés 
                            sous forme d'un entier par pixel. 
                            L'entier vaut entre 0 (noir) et 255 (blanc)
        """
        ...
        # Compléter ici
        ...
        return tableau_sortie
    ```

    ```python title="Test" linenums="1"
    assert transforme_vers_ng([(10,10,10), (30,30,30), (20,20,20)]) == [10,30,20]
    ```

!!! bug "Test" 
    Pour tester uniquement cette partie vous pouvez exécuter:
    ``` batch title="Terminal" linenums="1"
    python challenge.py -g
    ```