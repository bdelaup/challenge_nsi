# Challenge 2 : Noir & Blanc

| Originale  | ![arrow](../img/right_arrow.svg){ width="20"} | Transformée |
| ---------- | --------------- | ---------- |
|![Originale](img/champs_ng.jpg){ width="100"}| ![arrow](../img/right_arrow.svg){ width="20"}  |![Transformée](img/nb_champs_ng.jpg){ width="100" }|

!!! Question "A faire"
    Transformer une image couleur au format en image niveau de gris vers une noir et blanc.

!!! Tips "Démarche"
    1. Éditer le fichier ```fonctions.py``` 
    2. Ajouter la fonction ```transforme_vers_ng()``` 
    3. Ajouter au moins un test
    
!!! Example "Exemple" 
    ```python title="Prototype transforme_vers_nb()" linenums="1"
    def transforme_vers_ng(tableau_entree):
        """ 
        Transforme une image en niveau de gris vers une image noir et blanc
        tableau_entree : liste de tuple.
                            Chaque tuple code un pixel au format niveau de gris : 0 (noir) , 255 (blanc).

        retourne :       une liste de pixels codés 
                            sous forme d'un entier par pixel. 
                            L'entier vaut entre 0 (noir) et 1 (blanc)
        """
        ...
        # Compléter ici
        ...
        return tableau_sortie
    ```

    ```python title="Test" linenums="1"
    assert transforme_vers_nb([10, 200, 100]) == [0,1,0]
    ```

!!! bug "Test"
    Pour tester uniquement cette partie vous pouvez exécuter:
    ``` batch title="Terminal" linenums="1"
    python challenge.py -n
    ```