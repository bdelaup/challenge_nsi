# Challenge 3 : Histogramme d'exposition

| Originale  | ![arrow](../img/right_arrow.svg){ width="20"} | Transformée |
| ---------- | --------------- | ---------- |
|![Originale](img/champs_ng.jpg){ width="100"}| ![arrow](../img/right_arrow.svg){ width="20"}  |![Transformée](img/hist_champs_ng.jpg){ width="100" }|

L'histogramme d'une photo permet de savoir si l'équilibre entre les zones claires et les zones sombres est satisfaisant.

> Pour en savoir plus : [cliquer ici](https://avecunphotographe.fr/histogramme-en-photo/) 

!!! Question "A faire"
    Générer l'histogramme d'une image en niveau de gris.

!!! Tips "Démarche"
    1. Éditer le fichier ```fonctions.py``` 
    2. Ajouter la fonction ```niveau_de_gris_vers_histogramme``` 
    3. Ajouter au moins un test
    
!!! Example "Exemple"
    ```python title="Prototype transforme_vers_nb()" linenums="1"
    def niveau_de_gris_vers_histogramme(tableau_entree):
        """ 
        Génère un tableau de 256 entier. 
        Le premier élément du tableau contient le nombre de pixel à 0.
        Le deuxième élément, le nombre de pixel à 1.
        ...
        Le dernier élément, le nombre de pixel à 255

        tableau_entree : liste d'entier'.
                            Chaque tuple code un pixel au format niveau de gris : 
                            0 (noir) , 255 (blanc).

        retourne :       Une liste d'entier
                            Chaque élément code pour le nombre de pixel 
                            ayant l'intensité correspondante.
        """
        ...
        # Compléter ici
        ...
        return tableau_sortie
    ```

    ```python title="Test" linenums="1"
    res_test = [0]*256
    res_test[10] = 2
    res_test[200] = 1

    for i in range (255):
        assert niveau_de_gris_vers_histogramme([10, 200, 10])[i] == res_test[i]
    ```

!!! bug "Tests"
    Pour tester uniquement cette partie vous pouvez exécuter:
    ``` batch title="Terminal" linenums="1"
    python challenge.py -b
    ```
