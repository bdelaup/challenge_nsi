# Accueil

## Le défi
L'objectif est de réaliser des fonctions de traitement d'image permettant de réaliser les transformations suivantes :

| Originale | Niveau de gris | N & B | Histogramme | Créatif |
| --------- | -------------- | ----- | ----------- | ------- |
|![RVB](challenges/img/champs_rgb.jpg){ width="100"}|![NG](challenges/img/champs_ng.jpg){ width="100" }|![NB](challenges/img/nb_champs_ng.jpg){ width="100" }|![HIST](challenges/img/hist_champs_ng.jpg){ width="100" }|![CREATIF](challenges/img/creatif_champs_rgb.jpg){ width="100" }|

1. Construire des images en niveau de gris, et en noir et blanc à partir d'images couleurs.
2. Générer l'histogramme d'exposition à partir d'une image en niveau de gris.
3. Créer un filtre créatif d'une image.

## Comment procéder

1. Lire (et relire) la partie sur les images numériques et leurs codages (voir introduction).
2. Télécharger les ressources élèves
3. Réaliser les activités. Le filtre créatif est à réaliser en dernier.


L'archive des ressources contient les fichiers suivants :
```
    eleves
    ├── images 
    │   ├── champs_ng.jpg
    |   ├── champs_rgb.jpg
    │   ├── fleur_ng.jpg
    |   ├── fleur_rgb.jpg
    │   ├── ville_ng.jpg
    |   └── ville_rgb.jpg
    ├── fonctions.py
    └── challenge.py
```

Pour exécuter votre programme il vous suffira d'exécuter le fichier `challenge.py`. Ce programme chargera votre code comme un greffon (plugin). Votre code sera à écrire dans le fichier `fonctions.py`

!!! hint "Mantra du jour"
    *Avant de coder,*<br>
    *Je fais sur papier*<br>
    *Pour qu'on puisse m'aider.*


***Bonne chance, bon courage et bonne humeur à tous !***


## Les lycées participants 

| Jules Haag                                 | Victor Hugo                                |
| ------------------------------------------ | ------------------------------------------ |
| ![JH](img/jh.png){ width="100" align=left} | ![JH](img/vh.png){ width="100" align=left} |

