# Couleurs et pixels

## Perception par l'œil de la lumière.

En physique on caractérise une couleur par une longueur d’onde.

Idéalement, pour avoir le plus d'information sur une lumière, l'œil devrait être capable de mesurer le spectre entier, c'est-à-dire qu'il devrait avoir différents détecteurs de lumière, chacun sensible à une longueur d'onde unique, avec l'ensemble des détecteurs couvrant toutes les longueurs d'onde. En pratique, ce n'est pas le cas, ce qui complique un peu le concept de "couleur".


!!! note 
	[![1](1.png){ width="400" align=left}](1.png)
	Les détecteurs de l'œil humain sont des cônes, et il n'y en a que trois types : S, M et L qui ont un maximum de sensibilité à ces longueurs d’onde : 

	* S : 440 nm 
	* M : 540 nm 
	* L : 560 nm



Chaque type de cône est également sensible à des longueurs d'onde proches de son maximum de sensibilité comme le montre le graphe suivant. Ceci permet à l'œil de détecter de la lumière dans un large spectre de longueurs d'onde et pas seulement à 440, 540 et 560 nm.

!!! Note
	[![1](2.jpg){ width="300" align=left}](2.jpg) 
	Les bâtonnets servent à la vision nocturne, ils ne différencient pas les couleurs. Ils réagissent à une faible intensité lumineuse. C’est pour cela que « la nuit tous les chats sont gris ».


Lorsqu'une lumière tape un endroit de la rétine de l'œil, elle excite les cônes et transmet une information au cerveau. De façon simplifiée, cette information est représentée par trois nombres qui sont la réponse de chaque type de cône à la lumière. 

!!! example "Exemples"

	Une lumière faite d'une seule longueur d'onde de 420 nm n'excitera, d'après le graphique ci-dessus, quasiment que les photorécepteurs S. Cette information sera perçue comme une couleur bleue.

	Une lumière faite d'une seule longueur d'onde de 600 nm excitera principalement les photorécepteurs L et, dans une moindre mesure, M. Comme les capteurs sont excités différemment de l'exemple précédent la couleur perçue est différente. Ici, ce sera rouge-orangé.

Le fait de n'avoir que trois capteurs limite les capacités de l’œil : différents spectres lumineux peuvent être perçus comme ayant la même couleur.

!!! note
	[![1](3.png){ width="300" align=left}](3.png)

	 Par exemple les deux spectres suivants activent tous les deux les récepteurs S, M et L de la même façon et la couleur perçue est jaune dans les deux cas.


Ceci est à la fois un inconvénient et un avantage. L'inconvénient est qu'on ne peut pas différencier certaines combinaisons de longueurs d'onde, ce qui limite notre perception du monde qui nous entoure.

L'avantage, est que comme nous n'avons que trois capteurs, il est possible de générer toutes les couleurs observables avec seulement trois lumières. C'est le principe des écrans en couleur des télévisions, téléphones, ordinateurs et panneaux lumineux, où chaque point d'une image (pixel) est affiché en utilisant trois petites sources lumineuses, rouge, verte et bleue.

!!! note
	[![1](4.png){ width="200" align=left}](4.png)
	Pour obtenir des couleurs différentes on fera varier l’intensité lumineuse de chaque source lumineuse de chaque pixel. (pixel = contraction du terme anglais « picture element »)

## Reconstitution d’une image avec un écran.

Un écran est composé de milliers, voire de millions de pixels. 

!!! Note 
	[![1](5.png){ width="150" align=left}](5.png)
	Ici 1024 x 768 = 786 432 pixels

Un peu de technologie :


|LCD  |OLED  |OLED  |
|---------|---------|---------|
|[![1](6.png){ width="150" align=left}](6.png)     |    [![1](7.png){ width="150" align=left}](7.png)      |   [![1](8.png){ width="150" align=left}](8.png)       |


Pour ces trois images, la zone de l’écran grossi est blanche lorsque l’on regarde l’écran à distance normale. (rouge, vert et bleu sont ‘allumés’).



##	Dans la mémoire de l’ordinateur ?
### En couleur 
De nos jours, l’immense majorité des écrans sont dit ‘couleurs’, voire on ne dit rien car c’est devenu la norme. Donc les outils informatiques fonctionnent en couleur.
C’est dans la RAM de la carte graphique que l’on va stocker l’information qui fabrique l’image de l'écran. 

L’intensité de chaque couleur (R,V,B) de chaque pixel  est codé sur un octet en binaire naturel, soit un entier de 0 à 255, donc 3 octets pour un pixel.

Pour obtenir un pixel noir : R, V, B = (0, 0, 0), c’est la couleur de l’écran éteint.

Pour obtenir un pixel blanc : R, V, B = (255, 255, 255), l’intensité de chaque couleur est au maximum.

Quelques couleurs : (ou bien « couleur rvb dans un moteur de recherche »).

[![1](9.png){ width="400"}](9.png)

### En niveau de gris

Pour obtenir un niveau de gris avec un système « RVB », il suffit de mettre la même valeur d’intensité pour chaque couleur rouge, verte et bleu.

Les niveaux de gris vont du noir au blanc : voici quelques exemples de gris.

[![1](10.png){ width="400" }](10.png)


### En noir et blanc

Dans une image en noir et blanc on utilise plus que deux états : 
soit le noir (R, V, B) = (0, 0, 0) soit le blanc (R, V, B) = (255, 255, 255). 

Encore utilisé pour l’impression de certains journaux papier.


## Stockage des images dans un fichier

On peut stocker les 3 couleurs de chaque pixel de façon systématique, ce qui donne des fichiers de taille importante.

Exemples de fichiers qui utilisent cette méthode : 

- BMP (Bitmap) – extension .bmp
- TIFF (Tagged Image File Format) – extension .tif 
- PSD (Photoshop Document) – extension .psd


Afin de diminuer la taille des fichiers images on utilise des algorithmes de compression, avec plus ou moins de perte d’information sur l’image de base (nombre de couleurs, contours).

Exemples de fichiers qui utilisent la compression : 

- JPEG (Joint Photographic Experts Group) - extensions .jpg ou .jpeg 
- GIF (Graphics Interchange Format) – Extension .gif 
- PNG (Portable Network Graphics) - extension .png

Lorsque l’on charge un fichier de type image, le logiciel utilisé va recalculer tous les pixels dont l’écran a besoin pour afficher celle-ci correctement.

## Pour le challenge NSI

On va vous proposer de modifier des images que l’on vous fournit en format JPEG.

Deux fichiers sont mis à votre disposition :

- `challenge.py` sur lequel vous n'avez pas à intervenir et qui sert à :
	- gérer les dossiers
	- l'ouverture des images 
	- la transformation en liste
	- l'affichage des images transformées 
- `fonctions.py` qui sert à élaborer votre programme de modification d’image et là c’est à vous de jouer.

### Format des données codant une image couleur
De la forme `[(R0, V0, B0), (R1, V1, B1), …]`, le premier tuple correspond à l’intensité de chaque couleur R, V, B (variant de 0 à 255) du premier pixel de l’image et ainsi de suite jusqu’à la fin de l’image.

[![1](challenge_nsi2-rvb.jpg)](challenge_nsi2-rvb.jpg)

### Format des données codant une image en niveau de gris
De la forme `[G0, G1, …]` , le premier entier (vaut 0 à 255) correspond à l’intensité du premier pixel de l’image et ainsi de suite jusqu’à la fin de l’image.

[![1](challenge_nsi2-ng.jpg)](challenge_nsi2-ng.jpg)

### Format des données codant une image en noir et blanc
De la forme `[B0, B1, …]` , le premier entier (vaut 0 ou 1) correspond à la valeur du premier pixel de l'image et ainsi de suite jusqu’à la fin de l’image.

[![1](challenge_nsi2-nb.jpg)](challenge_nsi2-nb.jpg)


